let icons = document.querySelectorAll('.icon-password');

icons.forEach(function (icon) {
    if (icon.classList.contains('fa-eye-slash')) {
        icon.classList.remove('fa-eye-slash');
        icon.classList.add('fa-eye');
    }
    icon.addEventListener('click', (event) => {
        let input = event.target.parentNode.querySelector('input');
        if (input.getAttribute('type') == 'password') {
            input.setAttribute('type', 'text');
            event.target.classList.toggle('fa-eye-slash');
        } else {
            input.setAttribute('type', 'password');
            event.target.classList.toggle('fa-eye-slash');
        }
        console.log(event);
    });
});

let button = document.querySelector('.btn');
button.addEventListener('click', (event) => {
    event.preventDefault();
    let inputs = event.target.parentNode.querySelectorAll('input');
    if (inputs[0].value !== inputs[1].value) {
        alert('Нужно ввести одинаковые значения');
    } else {
        alert('You are welcome');
    };
});


